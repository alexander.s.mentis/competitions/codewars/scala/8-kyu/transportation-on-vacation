object Rental {

  def cost(days: Int): Int = 
    40 * days - (if (days >= 7) 50 
                 else if (days >= 3) 20 
                 else 0)
}